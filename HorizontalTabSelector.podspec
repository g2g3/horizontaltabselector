Pod::Spec.new do |s|

  s.name         = "HorizontalTabSelector"
  s.version      = "0.0.2"
  s.summary      = "A simple view for selecting between tabbed items."
  s.description  = <<-DESC
  					An easy to use custom view for selecting between tabbed items using a collectionview 
                   DESC

  s.homepage     = "https://bitbucket.org/g2g3/horizontaltabselector.git"
  s.license      = "MIT"
  s.author       = "G2G3"
  s.platform     = :ios
  s.ios.deployment_target = '9.0'
  s.source       = { :git => "https://bitbucket.org/g2g3/horizontaltabselector.git", :tag => "#{s.version}" }
  s.source_files  = "HorizontalTabSelector/**/*.{h,swift,m,xib}"
  s.resources = "HorizontalTabSelector/**/*.xib"
 
end
