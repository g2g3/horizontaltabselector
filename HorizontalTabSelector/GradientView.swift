//
//  GradientView.swift
//  HorizontalTabSelectorView
//
//  Created by Kieran Bamford on 13/04/2016.
//  Copyright © 2016 G2G3. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    private var gradientLayer: CAGradientLayer?
    
    @IBInspectable var startX: CGFloat = 0 {
        didSet {
            gradientLayer?.startPoint.x = startX
        }
    }
    
    @IBInspectable var startY: CGFloat = 0 {
        didSet {
            gradientLayer?.startPoint.y = startY
        }
    }
    
    @IBInspectable var endX: CGFloat = 1 {
        didSet {
            gradientLayer?.endPoint.x = endX
        }
    }
    
    @IBInspectable var endY: CGFloat = 0 {
        didSet {
            gradientLayer?.endPoint.y = endY
        }
    }
    
    @IBInspectable var startingColour: UIColor = UIColor.whiteColor() {
        didSet {
            if let gradient = gradientLayer {
                gradient.colors = [startingColour.CGColor, endingColour.CGColor]
            }
            else {
                setupGradient()
            }
        }
    }
    
    @IBInspectable var endingColour: UIColor = UIColor.blackColor() {
        didSet {
            if let gradient = gradientLayer {
                gradient.colors = [startingColour.CGColor, endingColour.CGColor]
            }
            else {
                setupGradient()
            }
        }
    }
    
    override var bounds: CGRect {
        didSet {
            gradientLayer?.frame = bounds
        }
    }
    
    private func setupGradient() {
        backgroundColor = UIColor.clearColor()
        gradientLayer = CAGradientLayer()
        gradientLayer?.backgroundColor = UIColor.clearColor().CGColor
        gradientLayer?.frame = bounds
        gradientLayer?.startPoint = CGPoint(x: startX, y: startY)
        gradientLayer?.endPoint = CGPoint(x: endX, y: endY)
        gradientLayer?.locations = [0.0, 0.8]
        gradientLayer?.colors = [startingColour.CGColor, endingColour.CGColor]
        layer.addSublayer(gradientLayer!)
    }
}
