//
//  OptionSelectorView.swift
//  HorizontalTabSelector
//
//  Created by Kieran Bamford on 19/04/2016.
//  Copyright © 2016 G2G3. All rights reserved.
//

import UIKit

public protocol HorizontalTabDelegate: class {
    func selectedTabAtIndex(index: Int)
}

@IBDesignable
public class HorizontalTabSelectorView: UIView {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var leftGradientView: GradientView!
    @IBOutlet private weak var rightGradientView: GradientView!
    
    @IBInspectable public var font: String = "System"
    @IBInspectable public var fontSize: CGFloat = 18
    
    @IBInspectable public var fadeOut: Bool = true {
        didSet {
            leftGradientView.hidden = !fadeOut
            rightGradientView.hidden = !fadeOut
        }
    }
    
    @IBInspectable public var gradientColour: UIColor? {
        didSet {
            if let gradientColour = gradientColour {
                leftGradientView.startingColour = gradientColour
                leftGradientView.endingColour = gradientColour.colorWithAlphaComponent(0.0)
                rightGradientView.startingColour = gradientColour
                rightGradientView.endingColour = gradientColour.colorWithAlphaComponent(0.0)
            }
        }
    }
    
    @IBInspectable public var selectedTextColor: UIColor = .whiteColor()
    @IBInspectable public var deselectedTextColor: UIColor = .grayColor()
    
    private var view: UIView!
    
    public weak var delegate: HorizontalTabDelegate?
    
    public var selectedRow = 0 {
        didSet {
            let indexPath = NSIndexPath(forRow: selectedRow, inSection: 0)
            self.collectionView?.selectItemAtIndexPath(indexPath, animated: false, scrollPosition: .None)
            delegate?.selectedTabAtIndex(selectedRow)
        }
    }
    
    public var names = [String]() {
        didSet {
            self.collectionView.reloadData()
            
            dispatch_async(dispatch_get_main_queue()) {
                if self.collectionView.numberOfItemsInSection(0) > 0 {
                    let firstIndexPath = NSIndexPath(forRow: 0, inSection: 0)
                    self.collectionView?.selectItemAtIndexPath(firstIndexPath, animated: false, scrollPosition: .CenteredHorizontally)
                    self.selectedRow = firstIndexPath.row
                }
            }
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
        
        collectionViewSetup()
    }
    
    private func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "HorizontalTabSelectorView", bundle: NSBundle(forClass: self.dynamicType))
        guard let view = nib.instantiateWithOwner(self, options: nil)[0] as? UIView else {
            return UIView()
        }
        
        return view
    }
    
    private func collectionViewSetup() {
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.registerNib(UINib(nibName: "HorizontalTabSelectorViewCell", bundle: NSBundle(forClass: self.dynamicType)), forCellWithReuseIdentifier: "optionCell")
        
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        self.setupLayout()
        
    }
    
    private func setupLayout() {
        let layout = collectionView.collectionViewLayout as! CenterCellCollectionViewFlowLayout
        let insetWidth = (self.view.frame.size.width - layout.itemSize.width) * 0.5
        
        layout.sectionInset.left = insetWidth
        layout.sectionInset.right = insetWidth
        layout.sectionInset.top = 0
        layout.sectionInset.bottom = 0
        
        layout.estimatedItemSize = CGSize(width: 30, height: 1)
        
        layout.delegate = self
        
        collectionView.setCollectionViewLayout(layout, animated: false)
    }
    
    override public func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        layoutIfNeeded()
        let layout = collectionView.collectionViewLayout as! CenterCellCollectionViewFlowLayout
        layout.minimumLineSpacing = collectionView.bounds.height
        
        if self.collectionView?.numberOfItemsInSection(0) > 0 {
            dispatch_async(dispatch_get_main_queue()) {
                let indexPath = NSIndexPath(forRow: self.selectedRow, inSection: 0)
                
                self.collectionView!.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: false)
            }
        }
        
    }
    
}

extension HorizontalTabSelectorView: UICollectionViewDelegateFlowLayout {
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        self.selectedRow = indexPath.row
    }
}

extension HorizontalTabSelectorView: UICollectionViewDataSource {
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return names.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("optionCell", forIndexPath: indexPath) as? HorizontalTabSelectorViewCell {
            cell.selectedColor = self.selectedTextColor
            cell.deselectedColor = self.deselectedTextColor
            
            if let font = UIFont(name: font, size: fontSize) {
                cell.nameLabel.font = font
            }
            cell.nameLabel.text = names[indexPath.row]
            
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension HorizontalTabSelectorView: CenterCellDelegate {
    func rowSelected(row: Int) {
        self.selectedRow = row
    }
}
