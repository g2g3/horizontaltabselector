//
//  OptionSelectorViewCell.swift
//  HorizontalTabSelector
//
//  Created by Kieran Bamford on 12/04/2016.
//  Copyright © 2016 G2G3. All rights reserved.
//

import UIKit

public class HorizontalTabSelectorViewCell: UICollectionViewCell {
    @IBOutlet weak public var nameLabel: UILabel!
    
    public var selectedColor: UIColor = UIColor.whiteColor()
    public var deselectedColor: UIColor = UIColor.lightGrayColor() {
        didSet {
            nameLabel.textColor = deselectedColor
        }
    }
    
    override public var selected: Bool {
        didSet {
            nameLabel.textColor = selected ? selectedColor : deselectedColor
        }
    }
}
