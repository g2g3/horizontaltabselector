//
//  CenterCellCollectionViewFlowLayout.swift
//  QualityBoard
//
//  Created by Kieran Bamford on 13/04/2016.
//  Copyright © 2016 G2G3. All rights reserved.
//

import UIKit

protocol CenterCellDelegate: class {
    func rowSelected(row: Int)
}

class CenterCellCollectionViewFlowLayout: UICollectionViewFlowLayout {
    weak var delegate: CenterCellDelegate?
    
    var selectedRow = 0 {
        didSet {
            delegate?.rowSelected(selectedRow)
        }
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth
            
            if let attributesForVisibleCells = self.layoutAttributesForElementsInRect(cvBounds) {
                
                var candidateAttributes: UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    
                    if attributes.representedElementCategory != UICollectionElementCategory.Cell {
                        continue
                    }
                    
                    if let candAttrs = candidateAttributes {
                        
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes
                        }
                        
                    }
                    else {
                        candidateAttributes = attributes
                        continue
                    }
                }
                
                if let candidateAttributes = candidateAttributes {
                    selectedRow = candidateAttributes.indexPath.row
                    return CGPoint(x: round(candidateAttributes.center.x - halfWidth), y: proposedContentOffset.y)
                }
            }
        }
        
        return super.targetContentOffsetForProposedContentOffset(proposedContentOffset)
    }
    
}
